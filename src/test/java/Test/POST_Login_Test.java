package Test;

import Base.Base;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;


import static io.restassured.RestAssured.given;

public class POST_Login_Test extends Base {
    public static HashMap map = new HashMap<>();

    @BeforeTest()
    public void BeforeMethod(){

        map.put("username","akyolg");
        map.put("password","Niso.2022!");
        map.put("applicationId",0);
        System.out.println(map);
    }

    @Test (priority = 1,description = "200 Success")
    public void POST_Create_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "/*+ token*/).
                contentType("application/json").
                body(map).
                when().
                post(LOGIN_PAGE_URL).
                then().
                statusCode(200).log().all();
    }

    @Test (priority = 2, description = "400 Not Success (Empty  Username)")
    public void POST_Create_Not_Success() throws InterruptedException {
        map.put("name",null);
        map.put("password",null);
        map.put("applicationId",0);
        System.out.println(map);

        given().headers("Authorization","Bearer ").
                contentType("application/json").
                body(map).
                when().
                post(LOGIN_PAGE_URL).
                then().
                statusCode(400).log().all();
    }
}

