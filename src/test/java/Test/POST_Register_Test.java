package Test;

import Base.Base;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;


import static io.restassured.RestAssured.given;

public class POST_Register_Test extends Base {
    public static HashMap map = new HashMap<>();

    @BeforeTest()
    public void BeforeMethod(){

/*        {
            "email": "test@niso.com.tr",
                "password": "12345678",
                "confirmPassword": "12345678",
                "firstName": "test",
                "lastName": "test",
                "applicationID": 1,
                "isCustomLogin": true
        }*/
        map.put("email","testtest@niso.com.tr");
        map.put("password","password1.!");
        map.put("confirmPassword","password1.!");
        map.put("firstName","Test");
        map.put("lastName", "Test");
        map.put("applicationID",0);
        map.put("isCustomLogin",true);
        System.out.println(map);
    }

    @Test (priority = 1,description = "200 Success")
    public void POST_Create_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "/*+ token*/).
                contentType("application/json").
                body(map).
                when().
                post(MAIN_PAGE_URL+"Register").
                then().
                statusCode(200).log().all();
    }

    @Test (priority = 2, description = "400 Not Success (Empty Company Name)")
    public void POST_Create_Not_Success() throws InterruptedException {
        map.put("email",null);
        map.put("password",null);
        map.put("confirmPassword", null);
        map.put("firstName",null);
        map.put("lastName", null);
        map.put("applicationID",1);
        map.put("isCustomLogin",false);
        System.out.println(map);

        given().headers("Authorization","Bearer ").
                contentType("application/json").
                body(map).
                when().
                post(MAIN_PAGE_URL + "Register").
                then().
                statusCode(400).log().all();
    }
}

