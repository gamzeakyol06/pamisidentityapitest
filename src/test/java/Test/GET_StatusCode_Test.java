package Test;
import Base.Base;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class GET_StatusCode_Test extends Base {

    @Test
    public void Test_ValidateToken() {
        given().headers("Authorization","Bearer ").
                contentType("application/json").
                when().
                get( MAIN_PAGE_URL+ "ValidateToken"+"?token="+doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL)).
                then().
                statusCode(200).log().all();
    }
    @Test
    public void Test_AccessTokenWithRefreshToken() {
        given().headers("Authorization","Bearer "+doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL)).
                contentType("application/json").
                when().
                get( MAIN_PAGE_URL+ "GetAccessTokenWithRefreshToken?token="+doPostRequestAuthorizeRefreshToken(LOGIN_PAGE_URL)).
                then().
                statusCode(200).log().all();
    }
}
