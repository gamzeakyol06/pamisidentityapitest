package Base;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.commons.lang3.RandomStringUtils;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import static io.restassured.RestAssured.*;

public class Base {

    //public final static String MAIN_PAGE_URL = "https://apitest.niso.dev/pamis/identity/v1/";
    //public final static String LOGIN_PAGE_URL = "https://apitest.niso.dev/pamis/identity/v1/Login";
    public final static String MAIN_PAGE_URL = "https://api.eldor.com.tr/pamis/identity";
    public final static String LOGIN_PAGE_URL = "https://api.eldor.com.tr/pamis/identity/Login";

    public static Response doGetRequest(String endpoint) {
        RestAssured.defaultParser = Parser.JSON;
        return given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                when().get(endpoint).
                then().contentType(ContentType.JSON).extract().response();
    }
    public static String doPostRequestAuthorizeValidateToken(String endpoint) {

        //String certificatesTrustStorePath = "/etc/alternatives/jre/lib/security/cacerts";
        //System.setProperty("javax.net.ssl.trustStore", certificatesTrustStorePath);
        //System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

        System.setProperty ("javax.net.ssl.trustStore", "NULL");
        System.setProperty ("javax.net.ssl.trustStoreType", "Windows-ROOT");

        HashMap map = new HashMap<>();

            map.put("username","akyolg");
            map.put("password","Niso.2022!");
            map.put("applicationId",0);
        System.out.println(map);
        RestAssured.defaultParser = Parser.JSON;
        Response responsetoken =  given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                body(map).
                when().
                post(endpoint).
                then().
                contentType(ContentType.JSON).extract().response();
        System.out.println(responsetoken);

        String token = responsetoken.jsonPath().getString("accessToken.code");
        System.out.println(token);
        return token;
    }
    public static String doPostRequestAuthorizeRefreshToken(String endpoint) {

        HashMap map = new HashMap<>();

        map.put("username","akyolg");
        map.put("password","Niso.2022!");
        map.put("applicationId",0);
        System.out.println(map);
        RestAssured.defaultParser = Parser.JSON;
        Response responsetoken =  given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                body(map).
                when().
                post(endpoint).
                then().
                contentType(ContentType.JSON).extract().response();
        System.out.println(responsetoken);

        String token = responsetoken.jsonPath().getString("refreshToken.code");
        System.out.println(token);
        return token;
    }
    public static List<String> doGetResponseName(@NotNull Response response) throws IOException {
        return response.jsonPath().getList("name");
    }
    public static List<Integer> doGetResponseListproductionLocationTypeID(@NotNull Response response) throws IOException {
        return response.jsonPath().getList("productionLocationTypeID");
    }
    public static List<Integer> doGetResponseListID(@NotNull Response response) throws IOException {
        return response.jsonPath().getList("id");
    }
    public static List<Integer> doGetResponseListParentID(@NotNull Response response) throws IOException {
        return response.jsonPath().getList("parentID");
    }
    public static List<Integer> doGetResponseListclientId(@NotNull Response response) throws IOException {
        return response.jsonPath().getList("clientId");
    }
    public static List<String> doGetResponseListtoken(@NotNull Response response) throws IOException {
        return response.jsonPath().getList("token");
    }

    public static String generateData(){
        String generateAlpha = RandomStringUtils.randomAlphanumeric(6);
        return generateAlpha;
    }
    public static String generateRandomDataforName(){
        String generateAlpha = RandomStringUtils.randomAlphanumeric(10);
        return generateAlpha;
    }
    public static Integer generateRandomDataforId(){
        Random rand = new Random();
        Integer generateAlphaNum = rand.nextInt(28);
        return generateAlphaNum;
    }
}
